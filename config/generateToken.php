<?php

return [
    'tokenTTL' => env('TOKEN_TTL'),
    'pathToPublicKey' => env('PATH_TO_PUBLIC_KEY'),
    'pathToPrivateKey' => env('PATH_TO_PRIVATE_KEY'),
];
