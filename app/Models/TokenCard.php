<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class TokenCard extends Model
{
    protected $table = 'token_cards';

    protected $hidden = [
        'token'
    ];

    protected $fillable = [
        'token'
    ];
}
