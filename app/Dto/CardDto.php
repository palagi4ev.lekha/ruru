<?php

namespace App\Dto;

class CardDto
{
    private string $pan;
    private string $cvc;
    private string $cardholder;
    private string $expire;
    private string $tokenExpire;

    public function __construct(string $pan, string $cvc, string $cardholder, string $expire)
    {
        $this->pan = $pan;
        $this->cvc = $cvc;
        $this->cardholder = $cardholder;
        $this->expire = $expire;
    }

    public function getPan(): string
    {
        return substr($this->pan, 0, 4) . '****' . substr($this->pan, 12, 4);
    }

    public function setTokenExpire(string $tokenExpire): void
    {
        $this->tokenExpire = $tokenExpire;
    }

    public function toJson(): string
    {
        return json_encode($this->toArray());
    }

    private function toArray(): array
    {
        return [
            'pan' => $this->pan,
            'cvc' => $this->cvc,
            'cardholder' => $this->cardholder,
            'expire' => $this->expire,
            'tokenExpire' => $this->tokenExpire,
        ];
    }
}
