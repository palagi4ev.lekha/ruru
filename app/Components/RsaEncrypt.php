<?php

namespace App\Components;

use App\Exceptions\EncodeException;
use App\Exceptions\FileNotFoundException;

class RsaEncrypt
{
    private string $pathToPublicKey;

    public function __construct(string $pathToPublicKey)
    {
        $this->pathToPublicKey = $pathToPublicKey;
    }

    /**
     * @throws FileNotFoundException
     * @throws EncodeException
     */
    public function encode(string $data): string
    {
        if (openssl_public_encrypt($data, $encrypted_data, $this->getKey($this->pathToPublicKey))) {
            return base64_encode($encrypted_data);
        }
        throw new EncodeException();
    }

    /**
     * @throws FileNotFoundException
     */
    private function getKey(string $path): string
    {
        if ($content = file_get_contents($path)) {
            return $content;
        }
        throw new FileNotFoundException();
    }
}
