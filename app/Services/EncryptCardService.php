<?php

namespace App\Services;

use App\Components\RsaEncrypt;
use App\Dto\CardDto;
use App\Exceptions\EncodeException;
use App\Exceptions\FileNotFoundException;
use App\Exceptions\SaveException;
use App\Models\TokenCard;

class EncryptCardService
{
    private int $tokenTtl;
    private RsaEncrypt $rsaEncrypt;

    public function __construct(int $tokenTtl, RsaEncrypt $rsaEncryptService)
    {
        $this->tokenTtl = $tokenTtl;
        $this->rsaEncrypt = $rsaEncryptService;
    }

    /**
     * @throws EncodeException
     * @throws FileNotFoundException
     * @throws SaveException
     */
    public function encrypt(CardDto $dto): string
    {
        $dto->setTokenExpire($this->generateTokenExpire());
        $token = $this->rsaEncrypt->encode($dto->toJson());
        if ($this->saveToken($token)) {
            return $token;
        }
        throw new SaveException();
    }

    private function generateTokenExpire()
    {
        return now()->addSeconds($this->tokenTtl)->timestamp;
    }

    private function saveToken(string $token): bool
    {
        $tokenCard = (new TokenCard());
        $tokenCard->token = $token;
        $tokenCard->created_at = now();
        return $tokenCard->save();
    }
}
