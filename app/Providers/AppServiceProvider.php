<?php

namespace App\Providers;

use App\Components\RsaEncrypt;
use App\Services\EncryptCardService;
use Illuminate\Contracts\Container\Container;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(EncryptCardService::class, function (Container $container) {
            return new EncryptCardService(
                config('generateToken.tokenTTL'),
                new RsaEncrypt(config('generateToken.pathToPublicKey'))
            );
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
