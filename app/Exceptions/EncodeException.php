<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class EncodeException extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $message = $message ?: 'Произошла ошибка кодирования';
        parent::__construct($message, $code, $previous);
    }
}
