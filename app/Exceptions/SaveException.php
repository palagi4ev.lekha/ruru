<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class SaveException extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $message = $message ?: "Не удалось сохранить токен";
        parent::__construct($message, $code, $previous);
    }
}
