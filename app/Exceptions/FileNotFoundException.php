<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class FileNotFoundException extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $message = $message ?: "Не удалось открыть файл";
        parent::__construct($message, $code, $previous);
    }
}
