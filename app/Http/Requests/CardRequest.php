<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property $pan
 * @property $cvc
 * @property $cardholder
 * @property $expire
 */
class CardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'pan' => 'required|string|luhn',
            'cvc' => ['required', 'numeric', 'regex:/[0-9][0-9][0-9]$/'],
            'cardholder' => 'required|string',
            'expire' => ['required', 'string', 'regex:/^(?:0?[1-9]|1[0-2])\/[1-9][0-9]$/'],
        ];
    }
}
