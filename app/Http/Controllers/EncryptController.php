<?php

namespace App\Http\Controllers;

use App\Dto\CardDto;
use App\Exceptions\EncodeException;
use App\Exceptions\FileNotFoundException;
use App\Exceptions\SaveException;
use App\Http\Requests\CardRequest;
use App\Services\EncryptCardService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class EncryptController extends Controller
{
    public function encrypt(EncryptCardService $service, CardRequest $request): JsonResponse
    {
        $dto = new CardDto($request->pan, $request->cvc, $request->cardholder, $request->expire);
        try {
            $token = $service->encrypt($dto);
            return response()->json([
                'success' => true,
                'pan' => $dto->getPan(),
                'token' => $token,
            ]);
        } catch (Exception $e) {
            Log::error($e);
            return response()->json(
                [
                    'success' => false,
                    'message' => 'Что-то пошло не так. Пожалуйста, попробуйте позже.'
                ],
                Response::HTTP_BAD_REQUEST
            );
        }
    }
}
